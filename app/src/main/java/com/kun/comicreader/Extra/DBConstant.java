package com.kun.comicreader.Extra;

public interface DBConstant {
    String TABLE_COMIC = "Comic";
    String ID = "Id";
    String C_NAME = "Name";
    String C_LINK = "Link";
}