package com.kun.comicreader;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kun.comicreader.Extra.Ultilites;
import com.kun.comicreader.Extra.ZoomImageView;
import com.kun.comicreader.Model.ModelInfo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReaderActivity extends AppCompatActivity {
    ViewPager viewPager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ModelInfo mData;
    private List<String> linkImage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        getData();
        initView();
        //setupSwipeLayout();
        loadData();
    }

    private void getData() {
        Intent intent = getIntent();
        mData = intent.getParcelableExtra("info");
    }

    private void setupSwipeLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                linkImage.clear();
                loadData();
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(mData.getName());
        viewPager = findViewById(R.id.viewpager);
        swipeRefreshLayout = findViewById(R.id.sw);
    }

    @SuppressLint("StaticFieldLeak")
    private void loadData() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                init(mData.getLink());
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                setTitle(mData.getName() + " (" + linkImage.size() + ")");
                viewPager.setAdapter(new ViewAdapter(linkImage.size()));
            }

        }.execute();
    }

    private void init(String link) {
        if (Ultilites.isConnectedInternet(ReaderActivity.this))
            try {
                Document doc = Jsoup.connect(link).get();
                Elements img = doc.select("img[src]");
                for (Element element : img) {
                    String res = element.attr("src");
                    String _res = res.toLowerCase();
                    if (_res.contains(".png") || _res.contains(".jpg")) {
                        if (!findContain(_res))
                            linkImage.add(res.replace("\r", ""));
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());

            }
    }

    private boolean findContain(String src) {
        String[] _lstBand = {"logo_2", "searchicon", "fb-share", "arrowleft",
                "arrowright"};

        for (String string : _lstBand) {
            if (src.contains(string))
                return true;
        }

        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class ViewAdapter extends PagerAdapter {
        @SuppressWarnings("unused")
        int size;
        private static final int MAX_WIDTH = 1024;
        private static final int MAX_HEIGHT = 768;

        public ViewAdapter(int size) {
            this.size = size;
        }

        @Override
        public int getCount() {
            return linkImage.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Log.d(">>>: ", "Image: " + linkImage.get(position));
            final ZoomImageView img = new ZoomImageView(ReaderActivity.this, null);
            img.setMaxZoom(4.0f);
            img.setScaleType(ImageView.ScaleType.MATRIX);

            Picasso.get().load(linkImage.get(position)).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    img.setImageBitmap(bitmap);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    img.setBackgroundResource(R.drawable.ic_noimage);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    img.setBackgroundResource(R.drawable.ic_load);
                }
            });


//            Picasso.get()
//                    .load(linkImage.get(position))
//                    .placeholder(R.drawable.ic_load)
//                    .error(R.drawable.ic_noimage)
//                    .into(img);
//            ImageLoader.getInstance().displayImage(linkImage.get(position),
//                    img, new ImageLoadingListener() {
//
//                        @Override
//                        public void onLoadingStarted(String arg0, View arg1) {
//                            img.setBackgroundResource(R.drawable.ic_load);
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String arg0, View arg1,
//                                                    FailReason arg2) {
//                            img.setBackgroundResource(R.drawable.ic_noimage);
//                            Log.e(ReaderActivity.class.getName(), ">>>:" + arg2.getCause().getMessage(), arg2.getCause());
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String arg0, View arg1,
//                                                      Bitmap arg2) {
//                            img.setBackgroundResource(android.R.color.transparent);
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String arg0, View arg1) {
//
//                        }
//
//                    });
            container.addView(img, 0);
            return img;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((ZoomImageView) object);
        }
    }

    public class BitmapTransform implements Transformation {

        private final int maxWidth;
        private final int maxHeight;

        public BitmapTransform(int maxWidth, int maxHeight) {
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
        }

        @Override
        public Bitmap transform(Bitmap source) {
            int targetWidth, targetHeight;
            double aspectRatio;

            if (source.getWidth() > source.getHeight()) {
                targetWidth = maxWidth;
                aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                targetHeight = (int) (targetWidth * aspectRatio);
            } else {
                targetHeight = maxHeight;
                aspectRatio = (double) source.getWidth() / (double) source.getHeight();
                targetWidth = (int) (targetHeight * aspectRatio);
            }

            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
            if (result != source) {
                source.recycle();
            }
            return result;
        }

        @Override
        public String key() {
            return maxWidth + "x" + maxHeight;
        }

    }
}
