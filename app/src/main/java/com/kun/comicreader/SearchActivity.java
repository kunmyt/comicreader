package com.kun.comicreader;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.kun.comicreader.Adapter.ComicAdapter;
import com.kun.comicreader.Extra.MarginDecoration;
import com.kun.comicreader.Extra.RecyclerItemClickListener;
import com.kun.comicreader.Model.ModelInfo;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView rc;
    private List<ModelInfo> lst = new ArrayList<>();
    private ComicAdapter adapter;
    private MaterialEditText _inputSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
        handleAction();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getString(R.string.action_search));
        _inputSearch = (MaterialEditText) findViewById(R.id.edit_query);
        rc = (RecyclerView) findViewById(R.id.rc);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rc.setLayoutManager(layoutManager);
        rc.setHasFixedSize(false);
        rc.addItemDecoration(new MarginDecoration());
        adapter = new ComicAdapter(SearchActivity.this, lst);
        rc.setAdapter(adapter);
    }

    private void handleAction() {
        rc.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SearchActivity.this, ListChapActivity.class);
                intent.putExtra("info", lst.get(position));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));

        _inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String query = _inputSearch.getText().toString();
                if (query.length() > 2) {
                    new Search(query).execute();
                } else {
                    lst.clear();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class Search extends AsyncTask<Void, Void, List<ModelInfo>> {

        private final String ROOT_QUERY_URL = "http://truyentranh.net/tim-kiem.tall.html?q=";
        private String _query;

        public Search(String query) {
            this._query = query;
        }

        @Override
        protected List<ModelInfo> doInBackground(Void... voids) {
            lst.clear();
            List<ModelInfo> _lst = new ArrayList<>();
            Document doc;
            try {
                doc = Jsoup.connect(ROOT_QUERY_URL + _query).get();
                Elements _er = doc.getElementsByClass("media-body");
                if (_er != null && _er.size() > 0)
                    for (Element element : _er) {
                        ModelInfo _info = new ModelInfo();
                        if (element.getElementsByTag("a").size() > 0) {
                            _info.setLink(element.getElementsByTag("a").get(0).attr("href"));
                            _info.setName(element.getElementsByTag("a").get(0).text());
                            _lst.add(_info);
                        }
                    }
                return _lst;
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ModelInfo> modelInfos) {
            if (modelInfos != null && modelInfos.size() > 0) {

                //TODO: Thử sắp xếp AB
                Collections.sort(modelInfos, new Comparator<ModelInfo>() {
                    @Override
                    public int compare(final ModelInfo object1, final ModelInfo object2) {
                        return object1.getName().compareTo(object2.getName());
                    }
                });

                lst.addAll(modelInfos);
                adapter.notifyDataSetChanged();
            }
        }
    }
}

