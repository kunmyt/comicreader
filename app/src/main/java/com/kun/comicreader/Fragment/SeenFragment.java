package com.kun.comicreader.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kun.comicreader.Adapter.ComicAdapter;
import com.kun.comicreader.DB.ComicEntity;
import com.kun.comicreader.Extra.MarginDecoration;
import com.kun.comicreader.Extra.RecyclerItemClickListener;
import com.kun.comicreader.ListChapActivity;
import com.kun.comicreader.Model.ModelInfo;
import com.kun.comicreader.R;

import java.util.ArrayList;
import java.util.List;

public class SeenFragment extends Fragment {
    private RecyclerView rc;
    private ComicAdapter adapter;
    private List<ModelInfo> lst = new ArrayList<>();

    public SeenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seen, container, false);
        initView(view);
        initData();
        handleAction();
        return view;
    }

    private void initView(View view) {
        rc = (RecyclerView) view.findViewById(R.id.rc);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(layoutManager);
        rc.setHasFixedSize(false);
        rc.addItemDecoration(new MarginDecoration());
        adapter = new ComicAdapter(getActivity(), lst);
        rc.setAdapter(adapter);
    }

    private void initData() {
        new AsyncTask<Void, Void, List<ModelInfo>>() {
            @Override
            protected List<ModelInfo> doInBackground(Void... voids) {
                return ComicEntity.getInstance(getActivity()).getAllFromTable();
            }

            @Override
            protected void onPostExecute(List<ModelInfo> _lst) {
                if (_lst != null && _lst.size() > 0) {
                    lst.addAll(_lst);
                    adapter.notifyDataSetChanged();
                }
            }
        }.execute();
    }

    private void handleAction() {
        rc.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ModelInfo _info = lst.get(position);
                Intent intent = new Intent(getActivity(), ListChapActivity.class);
                intent.putExtra("info", _info);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));
    }
}
