package com.kun.comicreader;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.kun.comicreader.Adapter.ComicAdapter;
import com.kun.comicreader.Extra.MarginDecoration;
import com.kun.comicreader.Extra.RecyclerItemClickListener;
import com.kun.comicreader.Model.ModelInfo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListChapActivity extends AppCompatActivity {
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<ModelInfo> lst = new ArrayList<>();
    private RecyclerView rc;
    private ComicAdapter adapter;
    private ModelInfo mData;
    private FloatingActionButton btnStart, btnEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listchap);

        getData();
        initView();
        setupSwipeLayout();
        initData();
        handleAction();
    }

    private void getData() {
        Intent intent = getIntent();
        mData = intent.getParcelableExtra("info");
    }

    private void initView() {
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(mData.getName());
        swipeRefreshLayout =  findViewById(R.id.sw);
        rc =  findViewById(R.id.rc);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rc.setLayoutManager(layoutManager);
        rc.setHasFixedSize(false);
        rc.addItemDecoration(new MarginDecoration());
        adapter = new ComicAdapter(ListChapActivity.this, lst);
        rc.setAdapter(adapter);
        btnStart =  findViewById(R.id.btnStart);
        btnEnd =  findViewById(R.id.btnEnd);
    }

    private void setupSwipeLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void initData() {
        lst.clear();
        new AsyncTask<Void, Void, List<ModelInfo>>() {
            private List<ModelInfo> x = new ArrayList<>();

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            protected List<ModelInfo> doInBackground(Void... voids) {
                try {
                    Document document = Jsoup.connect(mData.getLink()).get();
                    Elements getListChap = document.getElementById("examples").getElementsByTag("a");
                    for (Element chap : getListChap) {
                        ModelInfo info = new ModelInfo();
                        info.setName(chap.attr("title"));
                        info.setLink(chap.attr("href"));
                        info.set_Date(chap.getElementsByClass("date-release").text());
                        x.add(info);
                    }
                    return x;
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<ModelInfo> modelInfos) {
                swipeRefreshLayout.setRefreshing(false);
                if (modelInfos != null && modelInfos.size() > 0) {
                    lst.addAll(modelInfos);
                    adapter.notifyDataSetChanged();
                }
            }
        }.execute();
    }

    private void handleAction() {
        rc.addOnItemTouchListener(new RecyclerItemClickListener(ListChapActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!swipeRefreshLayout.isRefreshing()) {
                    Intent intent = new Intent(ListChapActivity.this, ReaderActivity.class);
                    intent.putExtra("info", lst.get(position));
                    startActivity(intent);
                }
            }
        }));

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rc.scrollToPosition(0);
            }
        });

        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lst.size() > 1)
                    rc.scrollToPosition(lst.size() - 1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
