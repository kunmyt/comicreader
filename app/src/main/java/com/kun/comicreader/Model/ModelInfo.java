package com.kun.comicreader.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kunmyt on 3/5/16.
 */
public class ModelInfo implements Parcelable {
    private String Name;
    private String Link;

    public String get_Date() {
        return _Date;
    }

    public void set_Date(String _Date) {
        this._Date = _Date;
    }

    private String _Date;

    protected ModelInfo(Parcel in) {
        Name = in.readString();
        Link = in.readString();
        _Date = in.readString();
    }

    public ModelInfo() {
    }

    public static final Creator<ModelInfo> CREATOR = new Creator<ModelInfo>() {
        @Override
        public ModelInfo createFromParcel(Parcel in) {
            return new ModelInfo(in);
        }

        @Override
        public ModelInfo[] newArray(int size) {
            return new ModelInfo[size];
        }
    };

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Name);
        parcel.writeString(Link);
        parcel.writeString(_Date);
    }
}
