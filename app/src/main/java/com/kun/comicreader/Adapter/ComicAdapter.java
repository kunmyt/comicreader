package com.kun.comicreader.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kun.comicreader.DB.ComicEntity;
import com.kun.comicreader.Model.ModelInfo;
import com.kun.comicreader.R;

import java.util.List;

/**
 * Created by kunmyt on 3/5/16.
 */
public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ViewHolder> {


    private List<ModelInfo> lst;
    private Context _context;

    public ComicAdapter(Context context, List<ModelInfo> lst) {
        this._context = context;
        this.lst = lst;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder != null) {
            String _name = lst.get(position).getName();
            boolean isExist = ComicEntity.getInstance(_context).checkExist(_name);
            if (isExist)
                holder.txtTitle.setText(_name + " - Reading");
            else
                holder.txtTitle.setText(_name);
            if (lst.get(position).get_Date() != null && !lst.get(position).get_Date().equals("")) {
                holder.txtDate.setVisibility(View.VISIBLE);
                holder.txtDate.setText(lst.get(position).get_Date());
            } else {
                holder.txtDate.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return lst.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtTitle, txtDate;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
        }
    }
}
