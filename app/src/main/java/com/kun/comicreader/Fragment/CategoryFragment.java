package com.kun.comicreader.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.kun.comicreader.Adapter.ComicAdapter;
import com.kun.comicreader.DB.ComicEntity;
import com.kun.comicreader.Extra.LinkedHashMapAdapter;
import com.kun.comicreader.Extra.MarginDecoration;
import com.kun.comicreader.Extra.RecyclerItemClickListener;
import com.kun.comicreader.Extra.Ultilites;
import com.kun.comicreader.ListChapActivity;
import com.kun.comicreader.Model.ModelInfo;
import com.kun.comicreader.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CategoryFragment extends Fragment {

    private final static String ROOT_URL = "http://truyentranh.net";
    private Spinner spinner;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rc;
    private ComicAdapter adapter;
    private List<ModelInfo> lst = new ArrayList<>();
    private String url_cate = "";
    private LinkedHashMapAdapter<String, String> adapterSpinner;
    private LinkedHashMap<String, String> mapData = new LinkedHashMap<>();


    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comic_cate, container, false);
        initView(view);
        initSpinner();
        setupSwipeLayout();
        handleAction();
        return view;
    }

    private void initView(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.sw);
        spinner = (Spinner) view.findViewById(R.id.spinnerCate);
        adapterSpinner = new LinkedHashMapAdapter<>(getActivity(), android.R.layout.simple_spinner_item, mapData);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);
        rc = (RecyclerView) view.findViewById(R.id.rc);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(layoutManager);
        rc.setHasFixedSize(false);
        rc.addItemDecoration(new MarginDecoration());
        adapter = new ComicAdapter(getActivity(), lst);
        rc.setAdapter(adapter);
    }

    private void setupSwipeLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lst.clear();
                initData(url_cate);
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    //Get commic of Category
    private void initData(final String url) {
        if (Ultilites.isConnectedInternet(getActivity()))
            new AsyncTask<Void, Void, List<ModelInfo>>() {
                private List<ModelInfo> x = new ArrayList<>();

                @Override
                protected void onPreExecute() {
                    swipeRefreshLayout.setRefreshing(true);
                }

                @Override
                protected List<ModelInfo> doInBackground(Void... voids) {
                    try {
                        Document document = Jsoup.connect(url).get();
                        Elements lstComic = document.getElementsByClass("media-body");
                        for (Element cm : lstComic) {
                            ModelInfo info = new ModelInfo();
                            info.setLink(cm.getElementsByTag("a").get(0).attr("href"));
                            info.setName(cm.getElementsByTag("a").get(0).attr("title"));
                            x.add(info);
                        }
                        return x;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(List<ModelInfo> modelInfos) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (modelInfos != null && modelInfos.size() > 0) {

                        //TODO: Thử sắp xếp AB
                        Collections.sort(modelInfos, new Comparator<ModelInfo>() {
                            @Override
                            public int compare(final ModelInfo object1, final ModelInfo object2) {
                                return object1.getName().compareTo(object2.getName());
                            }
                        });

                        lst.addAll(modelInfos);
                        adapter.notifyDataSetChanged();
                    }
                }
            }.execute();
        else
            Toast.makeText(getActivity(), "No connection!", Toast.LENGTH_SHORT).show();
    }

    //Get category
    private void initSpinner() {
        if (Ultilites.isConnectedInternet(getActivity()))
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    swipeRefreshLayout.setRefreshing(true);
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        Document document = Jsoup.connect(ROOT_URL).get();
                        Element categoryClass = document.getElementsByClass("category").get(0);
                        Elements lstCate = categoryClass.getElementsByTag("a");
                        for (Element e : lstCate) {
                            String l = e.attr("href");
                            String t = e.text();
                            mapData.put(l, t);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    adapterSpinner.notifyDataSetChanged();
                }
            }.execute();
        else
            Toast.makeText(getActivity(), "No connection!", Toast.LENGTH_SHORT).show();
    }

    private void handleAction() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Map.Entry<String, String> item = (Map.Entry<String, String>) spinner.getSelectedItem();
                url_cate = item.getKey();
                lst.clear();
                initData(url_cate);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        rc.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!swipeRefreshLayout.isRefreshing()) {

                    ModelInfo _info = lst.get(position);
                    boolean isExist = ComicEntity.getInstance(getActivity()).checkExist(_info.getName());
                    ComicEntity.getInstance(getActivity()).insertOrUpdate(_info, isExist);

                    Intent intent = new Intent(getActivity(), ListChapActivity.class);
                    intent.putExtra("info", _info);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }));
    }
}
