package com.kun.comicreader.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kun.comicreader.Extra.DBConstant;
import com.kun.comicreader.Extra.Ultilites;
import com.kun.comicreader.Model.ModelInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kun on 06-Aug-16.
 */
public class ComicEntity implements IDBManager<ModelInfo>, DBConstant {

    private static DatabaseHandler handler;
    private static ComicEntity instance = null;
    private SQLiteDatabase db;

    protected ComicEntity() {
    }

    public static ComicEntity getInstance(Context context) {
        if (instance == null)
            instance = new ComicEntity();
        handler = new DatabaseHandler(context);
        return instance;
    }

    @Override
    public boolean insertOrUpdate(ModelInfo info, boolean isInsert) {
        ContentValues values = new ContentValues();
        values.put(C_NAME, Ultilites.EncodeBase64(info.getName()));
        values.put(C_LINK, Ultilites.EncodeBase64(info.getLink()));
        if (!isInsert) {// Insert
            try {
                db = handler.getWritableDatabase();
                db.insert(TABLE_COMIC, null, values);
                db.close();
                return true;
            } catch (Exception e) {
                db.close();
                e.printStackTrace();
                return false;
            }
        } else {
            db = handler.getWritableDatabase();
            int i = db.update(TABLE_COMIC, values, C_NAME + "=?", new String[]{info.getName()});
            if (i > 0) {
                db.close();
                return true;
            } else {
                db.close();
                return false;
            }
        }
    }

    @Override
    public List<ModelInfo> getAllFromTable() {
        List<ModelInfo> lst = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_COMIC;
        try {
            db = handler.getWritableDatabase();
            Cursor c = db.rawQuery(query, null);
            if (c.moveToFirst()) {
                do {
                    ModelInfo info = new ModelInfo();
                    info.setName(Ultilites.DecodeBase64(c.getString(c.getColumnIndex(C_NAME))));
                    info.setLink(Ultilites.DecodeBase64(c.getString(c.getColumnIndex(C_LINK))));
                    lst.add(info);
                }
                while (c.moveToNext());
            }
            c.close();
            db.close();
        } catch (Exception e) {
            db.close();
            return null;
        }
        return lst;
    }

    @Override
    public ModelInfo getById(int id) {
        return null;
    }

    public ModelInfo getByName(String name) {
        ModelInfo info = new ModelInfo();
        String selectQuery = "Select * FROM " + TABLE_COMIC + " Where " + C_NAME + "= '" + name + "'";
        try {
            db = handler.getWritableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                info.setName(Ultilites.DecodeBase64(c.getString(c.getColumnIndex(C_NAME))));
                info.setLink(Ultilites.DecodeBase64(c.getString(c.getColumnIndex(C_LINK))));
                c.close();
                db.close();
                return info;
            } else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            db.close();
            return null;
        }
    }

    @Override
    public boolean delete(ModelInfo info) {
        db = handler.getWritableDatabase();
        return db.delete(TABLE_COMIC, C_NAME + "=" + Ultilites.EncodeBase64(info.getName()), null) > 0;
    }

    @Override
    public boolean checkExist(int id) {
        return false;
    }

    public boolean checkExist(String name) {
        String selectQuery = "SELECT * FROM " + TABLE_COMIC + " WHERE " + C_NAME + "= '" + Ultilites.EncodeBase64(name) + "'";
        try {
            db = handler.getWritableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            if (c.getCount() > 0) {
                c.close();
                db.close();
                return true;
            } else {
                c.close();
                db.close();
                return false;
            }
        } catch (Exception e) {
            db.close();
            e.printStackTrace();
            return false;
        }
    }
}
