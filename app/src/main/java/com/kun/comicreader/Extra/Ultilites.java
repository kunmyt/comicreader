package com.kun.comicreader.Extra;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

/**
 * Created by Kun on 10,March,2016
 * Viegrid JSC, Hanoi.
 */
public class Ultilites {
    public static boolean isConnectedInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return info != null && (info.isConnected());
    }

    public static String EncodeBase64(String s) {
        return s.equals("") ? s : Base64.encodeToString(s.getBytes(), Base64.DEFAULT);
    }

    public static String DecodeBase64(String s) {
        return s.equals("") ? s : new String(Base64.decode(s, Base64.DEFAULT));
    }
}
