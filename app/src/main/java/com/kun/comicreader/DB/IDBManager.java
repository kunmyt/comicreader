package com.kun.comicreader.DB;

import java.util.List;

/**
 * Created by Kun on 25,April,2016
 * Viegrid JSC, Hanoi.
 */
public interface IDBManager<T> {

    boolean insertOrUpdate(T info, boolean isInsert);

    List<T> getAllFromTable();

    T getById(int id);

    boolean delete(T info);

    boolean checkExist(int id);
}
