package com.kun.comicreader.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kun.comicreader.Adapter.ComicAdapter;
import com.kun.comicreader.DB.ComicEntity;
import com.kun.comicreader.Extra.MarginDecoration;
import com.kun.comicreader.Extra.RecyclerItemClickListener;
import com.kun.comicreader.Extra.Ultilites;
import com.kun.comicreader.ListChapActivity;
import com.kun.comicreader.Model.ModelInfo;
import com.kun.comicreader.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HomeFragment extends Fragment {

    private final static String ROOT_URL = "http://truyentranh.net/";
    private static String LIST_COMIC = "comic-feature-list";
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rc;
    private ComicAdapter adapter;
    private List<ModelInfo> lst = new ArrayList<>();
    private int type;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt("Type", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recycleview, container, false);
        initView(view);
        setupSwipeLayout();
        handleAction();
        initData();
        return view;
    }

    private void initView(View view) {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.sw);
        rc = (RecyclerView) view.findViewById(R.id.rc);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(layoutManager);
        rc.setHasFixedSize(false);
        rc.addItemDecoration(new MarginDecoration());
        adapter = new ComicAdapter(getActivity(), lst);
        rc.setAdapter(adapter);
    }

    private void setupSwipeLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                lst.clear();
                initData();
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void handleAction() {
        rc.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!swipeRefreshLayout.isRefreshing()) {

                    ModelInfo _info = lst.get(position);
                    boolean isExist = ComicEntity.getInstance(getActivity()).checkExist(_info.getName());
                    ComicEntity.getInstance(getActivity()).insertOrUpdate(_info, isExist);

                    Intent intent = new Intent(getActivity(), ListChapActivity.class);
                    intent.putExtra("info", _info);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }));
    }

    private void initData() {
        if (Ultilites.isConnectedInternet(getActivity()))
            new AsyncTask<Void, Void, List<ModelInfo>>() {
                private List<ModelInfo> x = new ArrayList<>();

                @Override
                protected void onPreExecute() {
                    swipeRefreshLayout.setRefreshing(true);
                }

                @Override
                protected List<ModelInfo> doInBackground(Void... voids) {
                    try {
                        Document document = Jsoup.connect(ROOT_URL).get();
                        switch (type) {
                            case 1:
                                Elements lstHot = document.getElementsByClass("hot-manga");
                                for (Element element : lstHot) {
                                    ModelInfo info = new ModelInfo();
                                    info.setLink(element.getElementsByTag("a").get(0).attr("href"));
                                    info.setName(element.getElementsByTag("a").get(0).attr("title"));
                                    x.add(info);
                                }
//                                Element element = document.getElementById(TOP_MENU);
//                                Elements elementsByTag = element.getElementsByTag("a");
//                                for (Element e : elementsByTag) {
//                                    ModelInfo info = new ModelInfo();
//                                    if (e.attr("href").contains("vechai.info")) {
//                                        String l = e.attr("href");
//                                        info.setLink(l);
//                                        info.setName(e.text());
//                                        x.add(info);
//                                    }
//                                }
                                break;
                            case 2:
                                Element element1 = document.getElementById(LIST_COMIC);
                                Elements elementsByClass = element1.getElementsByClass("StoryName");
                                for (Element e1 : elementsByClass) {
                                    ModelInfo info = new ModelInfo();
                                    info.setLink(e1.attr("href"));
                                    info.setName(e1.text());
                                    x.add(info);
                                }
                                break;
                            default:
                                break;
                        }
                        return x;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;

                    }
                }

                @Override
                protected void onPostExecute(List<ModelInfo> modelInfos) {
                    swipeRefreshLayout.setRefreshing(false);
                    if (modelInfos != null && modelInfos.size() > 0) {

                        //TODO: Thử sắp xếp AB
                        Collections.sort(modelInfos, new Comparator<ModelInfo>() {
                            @Override
                            public int compare(final ModelInfo object1, final ModelInfo object2) {
                                return object1.getName().compareTo(object2.getName());
                            }
                        });

                        lst.addAll(modelInfos);
                        adapter.notifyDataSetChanged();
                    }
                }
            }.execute();
        else
            Toast.makeText(getActivity(), "No connection!", Toast.LENGTH_SHORT).show();
    }
}
