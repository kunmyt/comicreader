package com.kun.comicreader.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kun.comicreader.Extra.DBConstant;

/**
 * Created by Kun on 25,April,2016
 * Viegrid JSC, Hanoi.
 */
public class DatabaseHandler extends SQLiteOpenHelper implements DBConstant {

    private static final String DATABASE_NAME = "comic.db";
    private static final int DATABASE_VERSION = 1;

    DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String CREATE_TABLE_COMIC = "CREATE TABLE " + TABLE_COMIC
                    + " (" + ID + " INTEGER PRIMARY KEY NOT NULL, "
                    + C_NAME + " TEXT, "
                    + C_LINK + " TEXT);";
            db.execSQL(CREATE_TABLE_COMIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DB", oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMIC);
        onCreate(db);
    }
}
